package com.michalrys.foobar;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FooBarTest {

    private static final String EXPECTED_REPRESENTATION = "1";
    private static final int NUMBER = 1;
    private static final int DIVIDABLE_BY_THREE = 3;
    private static final int DIVIDABLE_BY_FIVE = 5;
    private static final int DIVIDABLE_BY_FIFTEEN = 30;
    private FooBar fooBar;

    @Before
    public void setUp() throws Exception {

        fooBar = new FooBar();
    }

    @Test
    public void shouldReturnStringIntRepresentationWhenPutInt() {
        // given

        // when
        String result = fooBar.convertToString(NUMBER);

        // then
        Assert.assertEquals(EXPECTED_REPRESENTATION, result);
    }

    @Test
    public void shouldReturnStringFooWhenIntIsDividableByThree() {
        // given

        // when
        String result = fooBar.convertToString(DIVIDABLE_BY_THREE);

        // then
        Assert.assertEquals("Foo", result); // !uwaga: tutaj nie ma takiej duzej potrzeby wyciąania foo , bo w nazwie testu jest foo - duplikuje sie
    }

    @Test
    public void shouldReturnStringBarWhenIntIsDividableByFive() {
        // given

        // when
        String result = fooBar.convertToString(DIVIDABLE_BY_FIVE);

        // then
        Assert.assertEquals("Bar", result);
    }

    @Test
    public void shouldReturnStringBarWhenIntIsDividableByFifteen() {
        // given

        // when
        String result = fooBar.convertToString(DIVIDABLE_BY_FIFTEEN);

        // then
        Assert.assertEquals("FooBar", result);
    }


}
